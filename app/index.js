/*
 * Sortable grid - main entry point
 * Author: Keshant
 * Date: 24.08.2016
 */

var sortableGrid = angular.module("sortableGrid", ["ngRoute", "customDirective"]);

sortableGrid.config(["$routeProvider", function ($routeProvider) {
	
	$routeProvider
		.when("/", {templateUrl: "views/main.view.html", controller: "SortableDemo"});
}]);