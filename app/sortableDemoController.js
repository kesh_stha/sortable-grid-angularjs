/*
 * Sortable grid demo controller
 * Author: Keshant
 * Date: 24.08.2016
 */

function SortableDemo($scope) {

	$scope.data = [
		{id: 1, title: "First"},
		{id: 2, title: "Second"},
		{id: 3, title: "Third"},
		{id: 4, title: "Fourth"},
		{id: 5, title: "Fifth"},
		{id: 6, title: "Sixth"},
	];
}