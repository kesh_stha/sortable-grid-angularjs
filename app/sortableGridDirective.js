/*
 * Sortable grid (dependent on jQuery)
 * Author: Keshant
 * Date: 24.08.2016
 */

var customDirective = angular.module("customDirective", []);

customDirective.directive("sortableGrid", function() {
   
    return {
        restrict: 'EA',
        templateUrl: 'views/templates/grid.view.html',
        replace: true,
        scope: {
            data: '='
        },
        link: function (scope, elem, attr) {
            
            scope.$watch("data", function () {   
                // jquery dependencies
                $(function() {
                    $("#sortable").sortable({
                        cancel: ".ui-state-disabled",
                        placeholder: "ui-state-highlight"
                    });
                    $("#sortable").disableSelection();
                });
            }, true);            
        }
    };
});